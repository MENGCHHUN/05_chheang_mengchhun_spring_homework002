package com.example.hwmybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HwMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(HwMybatisApplication.class, args);
    }

}
