package com.example.hwmybatis.service;

import com.example.hwmybatis.model.entity.Invoice;

import java.util.List;

public interface InvoiceService {

    List<Invoice> getAllInvoice();
    Invoice getInvoiceById(Integer invoiceId);
}
