package com.example.hwmybatis.service;

import com.example.hwmybatis.model.entity.Customer;
import com.example.hwmybatis.model.request.CustomerRequest;
import com.example.hwmybatis.model.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer id);
    Customer deleteCustomerById(Integer id);
    Integer insertCustomer(CustomerRequest customerRequest);
    Integer updateCustomer (CustomerRequest customerRequest, Integer customer_id);
}
