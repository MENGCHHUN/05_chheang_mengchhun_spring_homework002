package com.example.hwmybatis.service.serviceimp;

import com.example.hwmybatis.model.entity.Customer;
import com.example.hwmybatis.model.request.CustomerRequest;
import com.example.hwmybatis.repository.CustomerRepository;
import com.example.hwmybatis.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomers();
    }

    @Override
    public Customer getCustomerById(Integer customer_id) {
        return customerRepository.findCustomerById(customer_id);
    }

    @Override
    public Customer deleteCustomerById(Integer customer_id) {
        return customerRepository.deleteCustomerById(customer_id);
    }

    @Override
    public Integer insertCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertCustomers(customerRequest);
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customer_id) {
        return customerRepository.updateCustomer(customerRequest, customer_id);
    }

}
