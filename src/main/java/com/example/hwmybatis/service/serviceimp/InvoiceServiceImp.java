package com.example.hwmybatis.service.serviceimp;

import com.example.hwmybatis.model.entity.Invoice;
import com.example.hwmybatis.repository.InvoiceRepository;
import com.example.hwmybatis.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;
    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice() ;
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.findInvoiceById(invoiceId);
    }
}
