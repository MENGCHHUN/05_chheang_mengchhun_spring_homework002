package com.example.hwmybatis.service.serviceimp;

import com.example.hwmybatis.model.entity.Product;
import com.example.hwmybatis.model.request.ProductRequest;
import com.example.hwmybatis.repository.ProductRepository;
import com.example.hwmybatis.service.ProductService;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;
    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer product_id) {
        return productRepository.findProductById(product_id);
    }

    @Override
    public Product deleteProductById(Integer product_id) {
        return productRepository.deleteProductById(product_id);
    }

    @Override
    public Integer insertProduct(ProductRequest productRequest) {
        return productRepository.insertProducts(productRequest);
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer product_id) {
        return productRepository.updateProducts(productRequest, product_id);
    }


}
