package com.example.hwmybatis.service;

import com.example.hwmybatis.model.entity.Product;
import com.example.hwmybatis.model.request.ProductRequest;

import java.util.ArrayList;
import java.util.List;

public interface ProductService {
    List<Product> getAllProducts ();
    Product getProductById(Integer id);
    Product deleteProductById(Integer id);
    Integer insertProduct(ProductRequest productRequest);
    Integer updateProduct (ProductRequest productRequest, Integer product_id);
//    List<Product> getAllProductForInner();
}
