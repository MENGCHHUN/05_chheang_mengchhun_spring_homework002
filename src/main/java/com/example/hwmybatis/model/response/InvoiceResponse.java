package com.example.hwmybatis.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private Timestamp date;
    private HttpStatus Status;
}
