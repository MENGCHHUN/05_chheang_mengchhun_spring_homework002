package com.example.hwmybatis.model.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    Integer customerId;
    String customerName;
    String customerPhone;
    String customerAddress;
}
