package com.example.hwmybatis.model.entity;

import lombok.*;
import org.apache.ibatis.annotations.Result;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private Integer productId;
    private String productName;
    private Double productPrice;

}
