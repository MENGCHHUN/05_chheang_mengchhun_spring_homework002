package com.example.hwmybatis.model.request;

import com.example.hwmybatis.model.entity.Customer;
import com.example.hwmybatis.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceRequest {
    private Timestamp invoiceDate;
    private Customer customer;
    private List<Product> products = new ArrayList<>();
}
