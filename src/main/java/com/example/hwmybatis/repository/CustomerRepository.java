package com.example.hwmybatis.repository;

import com.example.hwmybatis.model.entity.Customer;
import com.example.hwmybatis.model.request.CustomerRequest;
import com.example.hwmybatis.model.request.ProductRequest;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("SELECT * FROM customer_tb")
    @Results(
            id = "customerMapper",
            value = {
                    @Result(property = "customerId", column = "customer_id"),
                    @Result(property = "customerName", column = "customer_name"),
                    @Result(property = "customerPhone", column = "customer_phone"),
                    @Result(property = "customerAddress", column = "customer_address")
            }
    )
    List<Customer> findAllCustomers();

    @Select("SELECT * FROM customer_tb WHERE customer_id = ${customer_id} ")
    @Operation(summary = "Get Customer by Id")
    @ResultMap("customerMapper")
    Customer findCustomerById(Integer customer_id);

    @Select("DELETE FROM customer_tb WHERE customer_id = #{customer_id} " +
            "RETURNING customer_id")
    @Operation(summary = "Delete Customer by ID ")
    Customer deleteCustomerById(Integer customer_id);

    @Select("INSERT INTO customer_tb (customer_name, customer_phone, customer_address) " +
            "VALUES (#{request.customerName}, #{request.customerPhone}, #{request.customerAddress}) " +
            "RETURNING customer_id")
    @Results(
            id = "customerUpdateMapper",
            value = {
                    @Result(property = "customerId", column = "customer_id"),
            }
    )
    Integer insertCustomers(@Param("request") CustomerRequest customerRequest);

    @Select("UPDATE customer_tb SET " +
            "customer_name = #{request.customerName}, " +
            "customer_phone = #{request.customerPhone}," +
            "customer_address = #{request.customerAddress}" +
            " WHERE customer_id = #{customer_id} " +
            "RETURNING customer_id ")
    @ResultMap("customerUpdateMapper")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customer_id);

}
