package com.example.hwmybatis.repository;

import com.example.hwmybatis.model.entity.Product;
import com.example.hwmybatis.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    // =============================== Display all product =============================
    @Select("SELECT * FROM product_tb")
    @Results(
            id = "productMapper",
            value = {
                    @Result(property = "productId", column = "product_id"),
                    @Result(property = "productName", column = "product_name"),
                    @Result(property = "productPrice", column = "product_price")
            }
    )
    List<Product> findAllProduct();

    // ---------------------------------------------------------------------------------

    // =============================== Display product by Id =============================
    @Select("SELECT * FROM product_tb WHERE product_id = #{product_id}")
    @ResultMap("productMapper")
    Product findProductById(Integer product_id);

    // ---------------------------------------------------------------------------------

    // =============================== Delete product by Id =============================
    @Select("DELETE FROM product_tb WHERE product_id = #{product_id}")
    Product deleteProductById(Integer product_id);

    // ---------------------------------------------------------------------------------

    // =============================== Insert product  =================================
    @Select("INSERT INTO product_tb(product_name, product_price) VALUES (#{request.productName}, #{request.productPrice})" +
            "RETURNING product_id")
    @Results(
            id = "productUpdateMapper",
            value = {
                    @Result(property = "productId", column = "product_id"),
            }
    )
    Integer insertProducts(@Param("request") ProductRequest productRequest);

    // ---------------------------------------------------------------------------------

    // =============================== Insert product  =================================
    @Select("UPDATE product_tb SET " +
            "product_name = #{request.productName}, " +
            "product_price = #{request.productPrice}" +
            " WHERE product_id = #{product_id} " +
            "RETURNING product_id ")
    @ResultMap("productUpdateMapper")
    Integer updateProducts(@Param("request") ProductRequest productRequest, Integer product_id);

    // ---------------------------------------------------------------------------------

    @Select("SELECT p.product_id, p.product_name, p.product_price FROM invoice_detail_tb dt " +
            "INNER JOIN product_tb p ON p.product_id = dt.product_id " +
            "WHERE dt.invoice_id = #{invoiceId} ")
    @Result(property = "productId", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_price")
    List<Product> getProductByInvoiceId(Integer invoiceId);
}
