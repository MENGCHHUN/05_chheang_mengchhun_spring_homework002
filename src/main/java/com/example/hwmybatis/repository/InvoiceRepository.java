package com.example.hwmybatis.repository;

import com.example.hwmybatis.model.entity.Invoice;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface InvoiceRepository {

    @Select("SELECT * FROM invoice_td")
    @Results(
            id = "invoiceMapper",
            value = {
                    @Result(property = "invoiceId", column = "invoice_id"),
                    @Result(property = "invoiceDate", column = "invoice_date"),
                    @Result(property = "customer", column = "customer_id",
                    one = @One(select = "com.example.hwmybatis.repository.CustomerRepository.findCustomerById")
                    ),
                    @Result(property = "products", column = "invoice_id",
                      many = @Many(select = "com.example.hwmybatis.repository.ProductRepository.getProductByInvoiceId")
                    )
            }
    )
    ArrayList<Invoice> findAllInvoice();

    @Select("SELECT * FROM invoice_td WHERE invoice_id = #{invoice_id}")
    @ResultMap("invoiceMapper")
    Invoice findInvoiceById(Integer invoiceId);
}
