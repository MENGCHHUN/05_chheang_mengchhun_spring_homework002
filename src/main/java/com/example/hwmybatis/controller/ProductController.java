package com.example.hwmybatis.controller;

import com.example.hwmybatis.model.entity.Product;
import com.example.hwmybatis.model.request.ProductRequest;
import com.example.hwmybatis.model.response.ProductResponse;
import com.example.hwmybatis.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    @Operation(summary = "Get all products ")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct() {
        ProductResponse<List<Product>> response;
        ProductResponse<List<Product>> productResponse = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProducts())
                .message("Fetch all products successfully")
                .date(new Timestamp(System.currentTimeMillis()))
                .Status(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(productResponse);
    }

    @GetMapping("/get-product-by-id/{id}")
    @Operation(summary = "Get Product by ID")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer id) {
        ProductResponse<Product> productResponse;
        if (productService.getProductById(id) != null) {
            productResponse = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(id))
                    .message("This record has found successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.FOUND)
                    .build();
            return ResponseEntity.ok(productResponse);
        } else {
            productResponse = ProductResponse.<Product>builder()
                    .message("Product was not found")
                    .Status(HttpStatus.NOT_FOUND)
                    .date(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(productResponse);
        }

    }

    @DeleteMapping("/delete-product-by-id/{id}")
    @Operation(summary = "Delete Product By Id")
    public ResponseEntity<ProductResponse<Product>> deleteProductById(@PathVariable("id") Integer product_id) {
        ProductResponse<Product> productResponse = null;
        if (productService.getProductById(product_id) != null) {
            productResponse = ProductResponse.<Product>builder()
                    .payload(productService.deleteProductById(product_id))
                    .message("Delete Product Successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(productResponse);
        } else {
            productResponse = ProductResponse.<Product>builder()
                    .message("Product id was not found")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(productResponse);
        }
    }

    @PostMapping
    @Operation(summary = "Insert products")
    public ResponseEntity<ProductResponse<Product>> setProduct(@RequestBody ProductRequest productRequest) {
        Integer productId = productService.insertProduct(productRequest);
        if (productId != null) {
            ProductResponse<Product> productResponse = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("Insert successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(productResponse);
        }
        return null;
    }

    @PutMapping("/update-by-id/{id}")
    @Operation(summary = "Update Product by Id")
    public ResponseEntity<ProductResponse<Product>> updateProductById(@RequestBody ProductRequest productRequest, @PathVariable("id") Integer product_id){
        Integer productIdUpdate = productService.updateProduct(productRequest, product_id);
        System.out.println(productIdUpdate);
        ProductResponse<Product> productResponse = null;
        if(productIdUpdate != null){
            productResponse = ProductResponse.<Product> builder()
                    .payload(productService.getProductById(productIdUpdate))
                    .message("Update product succeccfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(productResponse);
        }else {
            return ResponseEntity.badRequest().body(productResponse);
        }
    }

}
