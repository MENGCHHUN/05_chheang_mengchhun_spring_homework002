package com.example.hwmybatis.controller;

import com.example.hwmybatis.model.entity.Invoice;
import com.example.hwmybatis.model.response.InvoiceResponse;
import com.example.hwmybatis.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController {
    private final InvoiceService invoiceService;
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    @Operation(summary = "Get all invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse<List<Invoice>> invoiceResponse;
        invoiceResponse = InvoiceResponse.<List<Invoice>> builder()
                .payload(invoiceService.getAllInvoice())
                .message("Fetch all invoice successfuly")
                .date(new Timestamp(System.currentTimeMillis()))
                .Status(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(invoiceResponse);
    }

    @GetMapping("/get-invoice-by-id/{id}")
    @Operation(summary = "Get invoice by ID")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> invoiceResponse;
        if(invoiceService.getInvoiceById(invoiceId) != null) {
            invoiceResponse = InvoiceResponse.<Invoice> builder()
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .message("Invoice was found")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(invoiceResponse);
        }else{
            invoiceResponse = InvoiceResponse.<Invoice> builder()
                    .message("Invoice was not found")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .Status(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(invoiceResponse);
        }

    }
}
