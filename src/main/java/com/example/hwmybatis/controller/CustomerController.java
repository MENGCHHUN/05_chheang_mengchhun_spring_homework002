package com.example.hwmybatis.controller;

import com.example.hwmybatis.model.entity.Customer;
import com.example.hwmybatis.model.request.CustomerRequest;
import com.example.hwmybatis.model.response.CustomerResponse;
import com.example.hwmybatis.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    @Operation(summary = "Get all customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer() {
        CustomerResponse<List<Customer>> customerResponse = null;
        customerResponse = CustomerResponse.<List<Customer>>builder()
                .payload(customerService.getAllCustomer())
                .message("Get all customer successfully")
                .date(new Timestamp(System.currentTimeMillis()))
                .status(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(customerResponse);
    }

    @GetMapping("/get-all-customer/{id}")
    @Operation(summary = "Get all customer")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customer_id) {

        CustomerResponse<Customer> customerResponse ;
        if (customerService.getCustomerById(customer_id) != null) {
            customerResponse = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(customer_id))
                    .message("Get all customer successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(customerResponse);
        } else {

            customerResponse = CustomerResponse.<Customer>builder()
                    .message("customer was not found")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.badRequest().body(customerResponse);

        }
    }

    @DeleteMapping("/delete-customer/{id}")
    @Operation(summary = "Delete Customer by ID")
    public ResponseEntity<CustomerResponse<Customer>> deleteCustomerById(@PathVariable("id" )Integer customer_id){
//        Integer customerId = customerService.deleteCustomerById(customer_id);
        CustomerResponse<Customer> customerResponse;
        if(customerService.getCustomerById(customer_id) != null){
            customerResponse = CustomerResponse.<Customer> builder()
                    .payload(customerService.deleteCustomerById(customer_id))
                    .message("Delete Customer successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(customerResponse);
        }else {
            customerResponse = CustomerResponse.<Customer>builder()
                    .message("Customer was not found")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.OK)
                    .build();
            return ResponseEntity.badRequest().body(customerResponse);
        }

    }
    @PostMapping
    @Operation(summary = "Insert Customer Information ")
    public ResponseEntity<CustomerResponse<Customer>> insertCustomer(
        @RequestBody CustomerRequest customerRequest
    ){
        Integer customerId = customerService.insertCustomer(customerRequest);
        CustomerResponse<Customer> customerResponse ;
        customerResponse = CustomerResponse.<Customer> builder()
                    .payload(customerService.getCustomerById(customerId))
                    .message("Insert Customer Successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(customerResponse);

    }

    @PutMapping("/update-customer/{id}")
    @Operation(summary = "Update Customer")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomer(
        @RequestBody CustomerRequest customerRequest,
        @PathVariable("id")  Integer customer_id
    ){
        Integer updateCustomerId = customerService.updateCustomer(customerRequest,customer_id);
        CustomerResponse<Customer> customerResponse ;
        if( updateCustomerId != null){
            customerResponse = CustomerResponse.<Customer> builder()
                    .payload(customerService.getCustomerById(updateCustomerId))
                    .message("Update Customer Successfully")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(customerResponse);
        }else {
            customerResponse = CustomerResponse.<Customer> builder()
                    .message("Customer was not found to update")
                    .date(new Timestamp(System.currentTimeMillis()))
                    .status(HttpStatus.NOT_FOUND)
                    .build();
            return ResponseEntity.ok(customerResponse);
        }
    }


}
